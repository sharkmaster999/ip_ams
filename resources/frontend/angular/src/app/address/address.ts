export interface IAddress {
    id: number,
    ip: string,
    label: string
}