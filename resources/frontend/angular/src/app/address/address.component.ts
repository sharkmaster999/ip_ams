import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { IAddress } from './address';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.css']
})
export class AddressComponent implements OnInit {

  private _url = 'http://localhost:8000/address';
  public addresses = <IAddress[]>{};

  constructor(private http:HttpClient) { }
  
  // Retrieve all addresses from API
  list() : Observable<IAddress[]>{
    return this.http.get<IAddress[]>(this._url);
  }

  ngOnInit(): void {
    this.list().subscribe(response => this.addresses = response);
  }

}
