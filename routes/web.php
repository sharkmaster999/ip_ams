<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AddressesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('address', [AddressesController::class, 'getAllAddress'])->middleware("cors");
Route::get('address/{id}', [AddressesController::class, 'getAddress']);
Route::post('address', [AddressesController::class, 'createAddress']);
Route::put('address/{id}', [AddressesController::class, 'updateAddress'])->middleware("cors");
