<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Addresses;

class AddressesController extends Controller
{
    public function getAllAddress() {
        $addresses = Addresses::get()->toJson(JSON_PRETTY_PRINT);
        return response($addresses, 200);
    }

    public function createAddress(Request $req) {
        $address = new Addresses;
        $address->ip = $req->ip;
        $address->label = $req->label;
        $address->save();

        return response()->json(["message" => "address created"], 201);
    }

    public function getAddress($id) {
        if(Addresses::where('id', $id)->exists()) {
            $address = Addresses::where('id', $id)->get()->toJson(JSON_PRETTY_PRINT);
            return response($address, 200);
        } else {
            return response(["message" => "no address found"], 400);
        }
    }

    public function updateAddress(Request $request, $id) {
        
    }
}
